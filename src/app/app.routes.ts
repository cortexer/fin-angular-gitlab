import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { SearchitemComponent } from './employees/searchitem.component';
import { SubrouteComponent } from './subroute/subroute.component';
import { ModuleWithProviders }  from '@angular/core';


export const ROUTES: Routes = [
  { path: '', component: HomeComponent },
  { path: 'searchitems', component: SearchitemComponent },
  { path: 'subroute', component: SubrouteComponent }
];