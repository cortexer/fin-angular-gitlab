import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { SubrouteComponent } from './subroute/subroute.component';

import { ROUTES } from './app.routes';
import { HomeComponent } from './home/home.component';
import {SearchitemComponent} from './employees/searchitem.component'

import { DataService } from './shared/data_services/data.service';
import { ConfigService } from './shared/api_settings/config.service';

@NgModule({
  declarations: [
    AppComponent,
    SubrouteComponent,
    HomeComponent,
	SearchitemComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(ROUTES)
  ],
  bootstrap: [AppComponent],
  providers:[DataService,ConfigService]
})
export class AppModule { }