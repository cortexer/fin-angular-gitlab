export interface ISearchItem {
     id?: number;
     nimi: string;
     email: string;
     opintopisteet: number;
     opiskelijanumero: number;
     selecting: boolean;
}

export interface IDiaryItem {
    id?: number;
    energyKcal: number;
    foodMass: number;
    foodName: string;
    food: number;
    energy: number;
    amount: number;
    grams: number;
    fatPer100g: number;
    carbsPer100g: number;
    proteinPer100g: number;
    totalFat: number;
    totalCarbs: number;
    totalProtein: number;
    selecting: boolean;
    info: string;
}

export interface IFoodItem {
     id?: number;
     name: any;
     productId: number;
     amount: number;
     grams: number;
     fat: number;
     carbohydrate: number;
     protein: number;
     units: Array<any>;
     energyKcal: number;
     foodMass: number;
     fatPer100g: number;
     carbsPer100g: number;
     proteinPer100g: number;
}
