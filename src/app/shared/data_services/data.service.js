"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
//Grab everything with import 'rxjs/Rx';
var Observable_1 = require('rxjs/Observable');
require('rxjs/add/operator/map');
require('rxjs/add/operator/catch');
require('rxjs/add/operator/toPromise');
var config_service_1 = require('../api_settings/config.service');
var DataService = (function () {
    function DataService(http, configService) {
        this.http = http;
        this.configService = configService;
        this.headers = new http_1.Headers({ 'Content-Type': 'application/json' });
        this._baseUrl = 'http://mongorest-h3677.rhcloud.com/';
        this._searchUrl = 'http://sym-h3677.rhcloud.com/testpage/';
        this._baseUrl = configService.getApiURI();
    }
    //************************************************************   DEPARTMENT SERVICES ************************************************************************ /
    //get all Departments
    DataService.prototype.getDepartments = function () {
        return this.http.get(this._baseUrl + 'department')
            .map(function (res) {
            return res.json();
        })
            .catch(this.handleError);
    };
    //get a specific department by Id
    DataService.prototype.getDepartment = function (id) {
        return this.http.get(this._baseUrl + 'department/' + id)
            .map(function (res) {
            return res.json();
        })
            .catch(this.handleError);
    };
    //get department's employees
    DataService.prototype.getDepartmentEmployees = function (id) {
        return this.http.get(this._baseUrl + 'department/' + id + '/employees')
            .map(function (res) {
            return res.json();
        })
            .catch(this.handleError);
    };
    //create a new Department
    DataService.prototype.createDepartment = function (name, description) {
        var body = JSON.stringify({ name: name,
            description: description });
        return this.http.post(this._baseUrl + 'department/', body, { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json().data; })
            .catch(this.handleError);
    };
    //updates a department
    DataService.prototype.updateDepartment = function (department) {
        var headers = new http_1.Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Access-Control-Allow-Origin', '*');
        return this.http.put(this._baseUrl + 'department/' + department.id, JSON.stringify(department), {
            headers: headers
        })
            .map(function (res) {
            return;
        })
            .catch(this.handleError);
    };
    //delete department
    DataService.prototype.deleteDepartment = function (id) {
        return this.http.delete(this._baseUrl + 'department/' + id)
            .map(function (res) {
            return;
        })
            .catch(this.handleError);
    };
    //************************************************************   EMPLOYEE SERVICES ************************************************************************ /
    //search
    DataService.prototype.searchItems = function (key) {
        return this.http.get(this._searchUrl + key + '/') //'https://api.myjson.com/bins/16u941'
            .map(function (res) {
            return res.json();
        })
            .catch(this.handleError);
    };
    //get all employees
    DataService.prototype.getEmployees = function () {
        return this.http.get(this._baseUrl + 'students')
            .map(function (res) {
            return res.json();
        })
            .catch(this.handleError);
    };
    //get single employee using Id
    DataService.prototype.getEmployee = function (_id) {
        return this.http.get(this._baseUrl + 'students/' + _id)
            .map(function (res) {
            return res.json();
        })
            .catch(this.handleError);
    };
    //create a new Employee
    DataService.prototype.createEmployee = function (nimi, email, opintopisteet, opiskelijanumero) {
        var body = JSON.stringify({ nimi: nimi,
            email: email,
            opintopisteet: opintopisteet,
            opiskelijanumero: opiskelijanumero });
        return this.http.post(this._baseUrl + 'students', body, { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json().data; })
            .catch(this.handleError);
    };
    //updates an employee
    DataService.prototype.updateEmployee = function (employee) {
        var headers = new http_1.Headers();
        headers.append('Content-Type', 'application/json');
        return this.http.put(this._baseUrl + 'students/' + employee.id, JSON.stringify(employee), {
            headers: headers
        })
            .map(function (res) {
            return;
        })
            .catch(this.handleError);
    };
    //delete employee
    DataService.prototype.deleteEmployee = function (id) {
        return this.http.delete(this._baseUrl + 'students/' + id)
            .map(function (res) {
            return;
        })
            .catch(this.handleError);
    };
    //create a new food
    DataService.prototype.createFood = function (product) {
        /// Delete this for the full data put
        var product = {
            productId: product.id,
            gramsdescription: product.gramsdescription,
            grams: product.grams,
            amountdescription: product.amountdescription,
            amount: product.amount
        };
        ///
        //let body = JSON.stringify({product}); useles???
        return this.http.post('http://mongorest-h3677.rhcloud.com/foods', product, { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json().data; })
            .catch(this.handleError);
    };
    //************************************************************   CONTRACT SERVICES ************************************************************************ /
    //get all contracts
    DataService.prototype.getContracts = function () {
        return this.http.get(this._baseUrl + 'contract')
            .map(function (res) {
            return res.json();
        })
            .catch(this.handleError);
    };
    //get single contract
    DataService.prototype.getContract = function (id) {
        return this.http.get(this._baseUrl + 'contract/' + id)
            .map(function (res) {
            return res.json();
        })
            .catch(this.handleError);
    };
    //delete contract
    DataService.prototype.deleteContract = function (id) {
        return this.http.delete(this._baseUrl + 'contract/' + id)
            .map(function (res) {
            return;
        })
            .catch(this.handleError);
    };
    //create a new Contract
    DataService.prototype.createContract = function (name, startDate, endDate, amount, employeeId) {
        var contract = JSON.stringify({
            name: name,
            startDate: startDate,
            endDate: endDate,
            amount: amount,
            employeeId: employeeId
        });
        return this.http.post(this._baseUrl + 'contract/', contract, { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json().date; })
            .catch(this.handleError);
    };
    //updates a contract
    DataService.prototype.updateContract = function (contract) {
        var headers = new http_1.Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Access-Control-Allow-Origin', '*');
        return this.http.put(this._baseUrl + 'contract/' + contract.id, JSON.stringify(contract), {
            headers: headers
        })
            .map(function (res) {
            return;
        })
            .catch(this.handleError);
    };
    //get all jobPositions
    DataService.prototype.getJobPositions = function () {
        var positions = [
            { name: 'Trainee', description: 'Trainee' },
            { name: 'Junior', description: 'Junior' },
            { name: 'Senior', description: 'Senior' },
            { name: 'Expert', description: 'Expert' },
            { name: 'Manager', description: 'Manager' }
        ];
        return positions;
    };
    DataService.prototype.handleError = function (error) {
        var applicationError = error.headers.get('Application-Error');
        var serverError = error.json();
        var modelStateErrors = '';
        if (!serverError.type) {
            console.log(serverError);
            for (var key in serverError) {
                if (serverError[key])
                    modelStateErrors += serverError[key] + '\n';
            }
        }
        modelStateErrors = modelStateErrors = '' ? null : modelStateErrors;
        return Observable_1.Observable.throw(applicationError || modelStateErrors || 'Server error');
    };
    DataService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http, config_service_1.ConfigService])
    ], DataService);
    return DataService;
}());
exports.DataService = DataService;
//# sourceMappingURL=data.service.js.map