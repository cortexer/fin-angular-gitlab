import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';

//Grab everything with import 'rxjs/Rx';
import { Observable } from 'rxjs/Observable';
import {Observer} from 'rxjs/Observer';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';

import {
    ISearchItem} from '../interfaces';
import { ConfigService } from '../api_settings/config.service';

@Injectable()
export class DataService {

    private headers = new Headers({'Content-Type': 'application/json'});

    _baseUrl: string = 'http://mongorest-h3677.rhcloud.com/';
    _searchUrl: string = 'http://sym-h3677.rhcloud.com/testpage/';

    constructor(private http: Http,
        private configService: ConfigService) {
        this._baseUrl = configService.getApiURI();
    }

    searchItems(key: string): Observable<ISearchItem[]> {
        return this.http.get('https://sym-h3677.rhcloud.com/testpage/' + key + '/') //'https://api.myjson.com/bins/16u941'
            .map((res: Response) => {
                return res.json();
            })
            .catch(this.handleError);
    }



    private handleError(error: any) {
    var applicationError = error.headers.get('Application-Error');
    var serverError = error.json();
    var modelStateErrors: string = '';

    if (!serverError.type) {
        console.log(serverError);
        for (var key in serverError) {
            if (serverError[key])
                modelStateErrors += serverError[key] + '\n';
        }
    }

    modelStateErrors = modelStateErrors = '' ? null : modelStateErrors;

    return Observable.throw(applicationError || modelStateErrors || 'Server error');
}

}
