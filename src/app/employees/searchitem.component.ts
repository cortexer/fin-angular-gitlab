import { Component, OnInit} from '@angular/core';
import { DataService } from '../shared/data_services/data.service';
import { ISearchItem } from '../shared/interfaces';
import { IFoodItem } from '../shared/interfaces';
import { IDiaryItem } from '../shared/interfaces';
//git push heroku master
@Component({
    moduleId: module.id,
    selector: 'searchitems',
    templateUrl: 'searchitem.component.html'
})
export class SearchitemComponent implements OnInit {

    Math: any;
    addFormOpen:boolean = false;
    searchitems: ISearchItem[] = [];
    pageButtons:any;
    dayButtons:any;
    allPageButtonsInit:string[] = new Array();
  	ISearchItem: ISearchItem;
    IDiaryItem: IDiaryItem;
  	diaryitems:IDiaryItem[];
  	weekNumber:number = 0;
  	nextDay:number = 10;
  	today:any = '';
  	diaryDate:any = '';
  	id:string = '';
  	grams:string = '';
  	info:string = '';
  	amount:string = '';
  	energy:string = '';
    keyword:string = '';
  	allSearchitems:any = new Array();
    dt: any = 0;
    totalCalories:number = 0;
    totalFat:number = 0;
    totalCarbs:number = 0;
    totalProtein:number = 0;

    constructor(private dataService: DataService) { }

    ngOnInit() {
        this.dt = new Date();
        this.weekNumber = this.ISO8601_week_no(this.dt);
        this.searchitems = [];
        this.diaryitems = [];
        this.allSearchitems = [];
        this.dayButtons = [];

        this.today = new Date();
        this.diaryDate = this.getFullDate(this.today.getDate());
        this.changeDay(this.diaryDate);
        this.Math = Math;
        this.keyword = 'testpage';
        this.dataService.searchItems(this.keyword).subscribe((searchitems:ISearchItem[]) => {
            this.allSearchitems = searchitems;
            this.searchitems = this.allSearchitems.slice(0, 5);
            this.createButtons();
            this.createDays();
        },
        error => {
            console.log('Failed to load items. '+error);
        })
    }

    ISO8601_week_no(dt){
       let tdt: any = new Date(dt.valueOf());
       let dayn = (dt.getDay() + 6) % 7;
       tdt.setDate(tdt.getDate() - dayn + 3);
       let firstThursday = tdt.valueOf();
       tdt.setMonth(0, 1);
       if (tdt.getDay() !== 4)
         {
        tdt.setMonth(0, 1 + ((4 - tdt.getDay()) + 7) % 7);
          }
       return 1 + Math.ceil((firstThursday - tdt) / 604800000);
    }


    getFullDate(dd):void{
      let today: any = new Date();

      let d = (1 + (this.weekNumber) * 7);
      let mm1 = new Date(2017, 0, d);
      let mm: any = JSON.stringify(mm1);
      let res = mm.split("-");
      mm = res[1];

      let yyyy = today.getFullYear();

      if(dd<10){
        dd='0'+dd;
      }
      /*if(mm<10){
        mm=''+mm;
      }*/

      today = this.weekNumber + '/' + dd;

      if (today.substring(0, 2) == '00') {
          today = today.substr(1);
      }

      return today;
}

  createDays():void{
  this.dayButtons = [];
  let firstPage = -5;
  let lastPage = 0;
  let weekday = new Array(7);
  weekday[0] = "Monday", weekday[1] = "Tuesday", weekday[2] = "Wednesday", weekday[3] = "Thursday";
  weekday[4] = "Friday", weekday[5] = "Saturday", weekday[6] = "Sunday";
  let activeDayIndex = 0;
  let currentDay = '';
  let dd = 0;
  for (let i = 0; i < 7; i++) {
   firstPage = firstPage + 5;
   lastPage = lastPage + 5;
   let today = new Date();
   today.setDate(today.getDate() - 1);
   let n = weekday[today.getDay()];
   let diaryDateActive = false;
   dd = today.getDate() - 2;
   let month = today.getMonth();
   if (n === weekday[i]) {
    diaryDateActive = true;
    dd = today.getDate();
    activeDayIndex = weekday.indexOf(weekday[i]);
    currentDay = n;
   }
   let dayButton = {
     id: i,
     date: dd,
     name: weekday[i],
     fulldate: this.getFullDate(dd),
     diaryDateActive: diaryDateActive
   };
   this.dayButtons.push(dayButton);
   }
   for (let i = 0; i < this.dayButtons.length; i++) {
      if (activeDayIndex > this.dayButtons.indexOf([i])) {
        dd = dd + 1;
        this.dayButtons[i].date = dd;
      }
      this.dayButtons[i].fulldate = this.getFullDate(this.dayButtons[i].date);
    }
    dd = 0;
    for (let i = 0; i < this.dayButtons.length; i++) {
          let index = i;
          dd = this.getDateOfWeek(this.weekNumber, 2017, index);
          this.dayButtons[i].date = dd;
          this.dayButtons[i].fulldate = this.getFullDate(dd);
     }
     for (let i = 0; i < this.dayButtons.length; i++) {
        if (this.dayButtons[i].diaryDateActive) {
          this.diaryDate = this.dayButtons[i].fulldate;
        }
     }
}

changeWeek(event):void {
     if (event.target.id === 'next') {
        this.weekNumber = this.weekNumber + 1;
     }
     if (event.target.id === 'previous') {
        this.weekNumber = this.weekNumber - 1;
     }
     this.createDays();
     this.changeDay(this.diaryDate);
 }

 getDateOfWeek(w, y, index):number {
   let d = (1 + (w - 1) * 7); // 1st of January + 7 days for each week

   let newDate = new Date(y, 0, d+1+index).toString();
   newDate = newDate.replace(/[^0-9]/g,'');
   newDate = newDate.substring(0, 2);
   let newDateInt = Number(newDate);
   return newDateInt;
   }

 changeDay(date):void {
     this.diaryDate = date;
     this.totalCalories = 0;
     this.totalFat = 0;
     this.totalCarbs = 0;
     this.totalProtein = 0;

     for (let i = 0; i < this.dayButtons.length; i++) {
       this.dayButtons[i].diaryDateActive = false;
        if (this.diaryDate === this.dayButtons[i].fulldate) {
         this.dayButtons[i].diaryDateActive = true;
        }
      }

     let storageItems:any = localStorage[this.diaryDate];
     if (storageItems) {
       storageItems = JSON.parse(storageItems);
       this.diaryitems = storageItems;
       for (let i = 0; i < this.diaryitems.length; i++) {
         this.totalCalories = this.totalCalories + this.diaryitems[i].energy;
         this.totalFat = this.totalFat + this.diaryitems[i].totalFat;
         this.totalCarbs = this.totalCarbs + this.diaryitems[i].totalCarbs;
         this.totalProtein = this.totalProtein + this.diaryitems[i].totalProtein;
         this.diaryitems[i].selecting = false;
         if (this.diaryitems[i].grams != 0) {
           this.diaryitems[i].info = this.diaryitems[i].grams + ' g';
         }
         if (this.diaryitems[i].amount != 0) {
           this.diaryitems[i].info = this.diaryitems[i].amount + ' portion(s)';
         }
       }
     } else {
       this.diaryitems = [];
     }
 }

    searchItems(filter:string){
      if (filter.length > 3) {
        this.dataService.searchItems(filter).subscribe((searchitems:ISearchItem[]) => {
            this.searchitems = [];
            this.allPageButtonsInit = [];
            this.pageButtons = [];
            this.allSearchitems = searchitems;
            this.searchitems = searchitems.slice(0, 5);
            this.createButtons();
            this.allPageButtonsInit = this.pageButtons;

            for (var i = 0; i < this.searchitems.length; i++) {
      //          this.searchitems[i].amount = 0;
      //          this.searchitems[i].grams = 0;
            }
          },
          error => {
              console.log('Failed: '+error);
          });
      }
  }

  createButtons():void{
  let buttonPagesAmount = this.allSearchitems.length / 5;
  buttonPagesAmount = Math.ceil(buttonPagesAmount);
  let firstPage = -5;
  let lastPage = 0;
  for (let i = 0; i < buttonPagesAmount; i++) {
   firstPage = firstPage + 5;
   lastPage = lastPage + 5;
   let pageButton = {
     id: i,
     firstPage: firstPage,
     lastPage: lastPage
   };
   this.pageButtons.push(pageButton);
   }
}

changePage(firstPage, filter):void {
   if (filter) {
     this.searchitems = this.allSearchitems.slice(firstPage, firstPage+5);
   } else {
     this.searchitems = this.allSearchitems.slice(firstPage, firstPage+5);
   }
 }

 changePortitionAmount(filter:string){
      console.log(filter);
}

changeGramAmount(filter:string){
      console.log(filter);
}

 checkSearchItem(searchitem:ISearchItem):void {
    if (searchitem.selecting) {
      searchitem.selecting = false;
    } else {
      searchitem.selecting = true;
    }
 }

 checkItem(diaryItem:IDiaryItem):void {
    if (diaryItem.selecting) {
      diaryItem.selecting = false;
    } else {
      diaryItem.selecting = true;
    }
 }

 addAmount(food:IFoodItem):void {
      food.productId = food.id;

      let productId = food.productId;
      let foodMass = food.units[1].mass;
      let diaryDate = this.diaryDate;

      let energy = 0;
      let totalFat = 0;
      let totalCarbs = 0;
      let totalProtein = 0;

      if (food.grams != 0) {
        energy = (food.grams / 100) * food.energyKcal;
        totalFat = (food.grams / 100) * food.fat;
        totalCarbs = (food.grams / 100) * food.carbohydrate;
        totalProtein = (food.grams / 100) * food.protein;
      }
      if (food.amount != 0) {
        energy = (foodMass * (food.amount / 100)) * food.energyKcal;
        totalFat = (foodMass * (food.amount / 100)) * food.fat;
        totalCarbs = (foodMass * (food.amount / 100)) * food.carbohydrate;
        totalProtein = (foodMass * (food.amount / 100)) * food.protein;
      }

      this.totalCalories = this.totalCalories + energy;
      this.totalFat = this.totalFat + totalFat;
      this.totalCarbs = this.totalCarbs + totalCarbs;
      this.totalProtein = this.totalProtein + totalProtein;

      let unixtimestamp = Math.round(+new Date()/1000);
      let storageItems:any = localStorage[diaryDate];

      let foodTest = <IDiaryItem>{};

      //PUT WEEK NUMBER AFTER FULL DATE SO IT WON'T BE DUPLICATE

      if (storageItems) {
            storageItems = JSON.parse(storageItems);
            foodTest = {
              id: unixtimestamp,
              energyKcal: food.energyKcal,
              foodMass: foodMass,
              foodName: food.name.fi,
              food: food.id,
              energy: energy,
              amount: food.amount,
              grams: food.grams,
              fatPer100g: food.fat,
              carbsPer100g: food.carbohydrate,
              proteinPer100g: food.protein,
              totalFat: totalFat,
              totalCarbs: totalCarbs,
              totalProtein: totalProtein,
              selecting: false,
              info: food.name.fi
            };
            storageItems.push(foodTest);
            localStorage[diaryDate] = JSON.stringify(storageItems);
          } else {
            let storageItems = new Array();
            foodTest = {
              id: unixtimestamp,
              energyKcal: food.energyKcal,
              foodMass: foodMass,
              foodName: food.name.fi,
              food: food.id,
              energy: energy,
              amount: food.amount,
              grams: food.grams,
              fatPer100g: food.fat,
              carbsPer100g: food.carbohydrate,
              proteinPer100g: food.protein,
              totalFat: totalFat,
              totalCarbs: totalCarbs,
              totalProtein: totalProtein,
              selecting: false,
              info: food.name.fi
            };
            storageItems.push(foodTest);
            localStorage[diaryDate] = JSON.stringify(storageItems);
          }
          this.diaryitems.push(foodTest);
          this.changeDay(this.diaryDate);
}

updateAmount(food:IFoodItem):void {
      food.productId = food.id;
      let productId = food.productId;
      let foodMass = food.foodMass;
      let id = food.id;
      this.totalCalories = 0;
      this.totalFat = 0;
      this.totalCarbs = 0;
      this.totalFat = 0;

      let energy = 0;
      let totalFat = 0;
      let totalCarbs = 0;
      let totalProtein = 0;

      if (food.grams != 0) {
        energy = (food.grams / 100) * food.energyKcal;
        totalFat = (food.grams / 100) * food.fatPer100g;
        totalCarbs = (food.grams / 100) * food.carbsPer100g;
        totalProtein = (food.grams / 100) * food.proteinPer100g;
      }
      if (food.amount != 0) {
        energy = (foodMass * (food.amount / 100)) * food.energyKcal;
        totalFat = (foodMass * (food.amount / 100)) * food.fatPer100g;
        totalCarbs = (foodMass * (food.amount / 100)) * food.carbsPer100g;
        totalProtein = (foodMass * (food.amount / 100)) * food.proteinPer100g;
      }

      for (let i = 0; i < this.diaryitems.length; i++) {
        if (this.diaryitems[i].id == id) {
            this.diaryitems[i].energy = energy;
            this.diaryitems[i].totalFat = totalFat;
            this.diaryitems[i].totalCarbs = totalCarbs;
            this.diaryitems[i].totalProtein = totalProtein;
          if (this.diaryitems[i].grams != 0) {
            this.diaryitems[i].info = this.diaryitems[i].grams + ' g';
          }
          if (this.diaryitems[i].amount != 0) {
            this.diaryitems[i].info = this.diaryitems[i].amount + ' portion(s)';
          }
          this.diaryitems[i].selecting = false;
        }
      this.totalCalories = this.totalCalories + this.diaryitems[i].energy;
      this.totalFat = this.totalFat + this.diaryitems[i].totalFat;
      this.totalCarbs = this.totalCarbs + this.diaryitems[i].totalCarbs;
      this.totalProtein = this.totalProtein + this.diaryitems[i].totalProtein;
      }

      localStorage[this.diaryDate] = JSON.stringify(this.diaryitems);

      this.changeDay(this.diaryDate);

}

removeItem(foodItem:IFoodItem):void {
    let id = foodItem.id;
    let storageItems:any = localStorage[this.diaryDate];

    this.totalCalories = 0;
    this.totalFat = 0;
    this.totalCarbs = 0;
    this.totalProtein = 0;
    if (storageItems) {
      storageItems = JSON.parse(storageItems);
      this.diaryitems = storageItems;
      for (let i = 0; i < this.diaryitems.length; i++) {
      this.totalCalories = this.totalCalories + this.diaryitems[i].energy;
      this.totalFat = this.totalFat + this.diaryitems[i].totalFat;
      this.totalCarbs = this.totalCarbs + this.diaryitems[i].totalCarbs;
      this.totalProtein = this.totalProtein + this.diaryitems[i].totalProtein;
      if (this.diaryitems[i].grams != 0) {
        this.diaryitems[i].info = this.diaryitems[i].grams + ' g';
      }
      if (this.diaryitems[i].amount != 0) {
        this.diaryitems[i].info = this.diaryitems[i].amount + ' portion(s)';
      }
      if (this.diaryitems[i].id == id) {
        this.diaryitems[i].info = 'getting removed!';
        this.diaryitems.splice(i, 1);
      }
      }
    }
    localStorage[this.diaryDate] = JSON.stringify(this.diaryitems);
    this.changeDay(this.diaryDate);
 }

}
