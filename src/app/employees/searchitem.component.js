"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var data_service_1 = require('../shared/data_services/data.service');
var SearchitemComponent = (function () {
    function SearchitemComponent(dataService) {
        this.dataService = dataService;
        this.addFormOpen = false;
        this.pageButtons = new Array();
        this.dayButtons = new Array();
        this.allPageButtonsInit = new Array();
    }
    SearchitemComponent.prototype.ngOnInit = function () {
        var _this = this;
        //let test = this.getDateOfWeek(35, 2017, 3);
        //console.log(test);
        var dt = new Date();
        console.log(this.ISO8601_week_no(dt));
        this.weekNumber = this.ISO8601_week_no(dt);
        var today = new Date();
        this.diaryDate = this.getFullDate(today.getDate());
        this.changeDay(this.diaryDate);
        this.Math = Math;
        this.id = 'omena';
        this.dataService.searchItems().subscribe(function (employees) {
            _this.allEmployees = employees;
            _this.allSearchitems = employees;
            _this.employees = employees.slice(0, 5);
            _this.employeesFilter = _this.allEmployees;
            _this.createButtons();
            _this.createDays();
            _this.allPageButtonsInit = _this.pageButtons;
            Materialize.toast('Employees loaded', 3000, 'green rounded');
        }, function (error) {
            Materialize.toast('Failed to load employees', 3000, 'red rounded');
            console.log('Failed to load employees.' + error);
        });
    };
    SearchitemComponent.prototype.ISO8601_week_no = function (dt) {
        var tdt = new Date(dt.valueOf());
        var dayn = (dt.getDay() + 6) % 7;
        tdt.setDate(tdt.getDate() - dayn + 3);
        var firstThursday = tdt.valueOf();
        tdt.setMonth(0, 1);
        if (tdt.getDay() !== 4) {
            tdt.setMonth(0, 1 + ((4 - tdt.getDay()) + 7) % 7);
        }
        return 1 + Math.ceil((firstThursday - tdt) / 604800000);
    };
    SearchitemComponent.prototype.searchItems = function (filter) {
        var _this = this;
        console.log(filter);
        if (filter.length > 3) {
            this.dataService.searchItems(filter).subscribe(function (searchitems) {
                _this.searchitems = [];
                _this.allEmployees = [];
                _this.allPageButtonsInit = [];
                _this.pageButtons = [];
                _this.allSearchitems = searchitems;
                _this.searchitems = searchitems.slice(0, 5);
                _this.employeesFilter = _this.allSearchitems;
                _this.createButtons();
                _this.allPageButtonsInit = _this.pageButtons;
                for (var i = 0; i < _this.searchitems.length; i++) {
                    _this.searchitems[i].amount = 0;
                    _this.searchitems[i].grams = 0;
                }
                console.log(_this.searchitems);
            });
        }
    };
    SearchitemComponent.prototype.fetchPosts = function () {
        var self = this;
        console.log('fetching!');
        fetch('https://sym-h3677.rhcloud.com/testpage/omena/', {
            method: 'GET',
            headers: {
                'Accept': '*'
            }
        }).then(function (result) { return result.json(); })
            .then(function (result) {
            console.log(result);
            //self.employees = result;
        });
    };
    SearchitemComponent.prototype.onSelect = function (employee) {
        this.selectedEmployee = employee;
    };
    /*
        getFullCurrentDate():void{
          let today = new Date();
          let dd = today.getDate();
          let mm = today.getMonth()+1;
          let yyyy = today.getFullYear();
          if(dd<10){
            dd='0'+dd;
          }
          if(mm<10){
            mm='0'+mm;
          }
          let today = dd+'/'+mm+'/'+yyyy;
    
          return today;
        }
    */
    SearchitemComponent.prototype.getFullDate = function (dd) {
        var today = new Date();
        var d = (1 + (this.weekNumber) * 7);
        var mm = new Date(2017, 0, d);
        mm = JSON.stringify(mm);
        var res = mm.split("-");
        mm = res[1];
        var yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0' + dd;
        }
        if (mm < 10) {
            mm = '' + mm;
        }
        var today = dd + '/' + mm + '/' + yyyy;
        if (today.substring(0, 2) == '00') {
            today = today.substr(1);
        }
        return today;
    };
    SearchitemComponent.prototype.changeWeek = function (event) {
        if (event.target.id === 'next') {
            this.weekNumber = this.weekNumber + 1;
        }
        if (event.target.id === 'previous') {
            this.weekNumber = this.weekNumber - 1;
        }
        this.createDays();
        this.changeDay(this.diaryDate);
    };
    SearchitemComponent.prototype.getDateOfWeek = function (w, y, index) {
        var d = (1 + (w - 1) * 7); // 1st of January + 7 days for each week
        var newDate = new Date(y, 0, d + 1 + index).toString();
        newDate = newDate.replace(/[^0-9]/g, '');
        return newDate = newDate.substring(0, 2);
    };
    SearchitemComponent.prototype.createDays = function (week) {
        this.dayButtons = [];
        var firstPage = -5;
        var lastPage = 0;
        var weekday = new Array(7);
        weekday[0] = "Monday", weekday[1] = "Tuesday", weekday[2] = "Wednesday", weekday[3] = "Thursday",
            weekday[4] = "Friday", weekday[5] = "Saturday";
        weekday[6] = "Sunday";
        var activeDayIndex = '';
        var currentDay = '';
        for (var i = 0; i < 7; i++) {
            firstPage = firstPage + 5;
            lastPage = lastPage + 5;
            var today = new Date();
            today.setDate(today.getDate() - 1);
            var n = weekday[today.getDay()];
            var diaryDateActive = false;
            var dd = today.getDate() - 2;
            var month = today.getMonth();
            if (n === weekday[i]) {
                diaryDateActive = true;
                dd = today.getDate();
                activeDayIndex = weekday.indexOf(weekday[i]);
                currentDay = n;
            }
            var dayButton = {
                id: i,
                date: dd,
                name: weekday[i],
                fulldate: this.getFullDate(dd, this.weekNumber),
                diaryDateActive: diaryDateActive
            };
            this.dayButtons.push(dayButton);
        }
        for (var i = 0; i < this.dayButtons.length; i++) {
            if (activeDayIndex > this.dayButtons.indexOf([i])) {
                dd = dd + 1;
                this.dayButtons[i].date = dd;
            }
            this.dayButtons[i].fulldate = this.getFullDate(this.dayButtons[i].date);
        }
        dd = 0;
        for (var i = 0; i < this.dayButtons.length; i++) {
            //          if ((month == 0 || month == 2 || month == 4 || month == 6 || month == 7 || month == 9 || month == 11) && this.dayButtons[i].date > 31) {
            var index = i;
            dd = this.getDateOfWeek(this.weekNumber, 2017, index);
            this.dayButtons[i].date = dd;
            this.dayButtons[i].fulldate = this.getFullDate(dd);
        }
        for (var i = 0; i < this.dayButtons.length; i++) {
            if (this.dayButtons[i].diaryDateActive) {
                this.diaryDate = this.dayButtons[i].fulldate;
            }
        }
    };
    SearchitemComponent.prototype.createButtons = function () {
        var buttonPagesAmount = this.allSearchitems.length / 5;
        buttonPagesAmount = Math.ceil(buttonPagesAmount);
        var firstPage = -5;
        var lastPage = 0;
        for (var i = 0; i < buttonPagesAmount; i++) {
            firstPage = firstPage + 5;
            lastPage = lastPage + 5;
            var pageButton = {
                id: i,
                firstPage: firstPage,
                lastPage: lastPage
            };
            this.pageButtons.push(pageButton);
        }
    };
    SearchitemComponent.prototype.changePortitionAmount = function (filter) {
        console.log(filter);
    };
    SearchitemComponent.prototype.changeGramAmount = function (filter) {
        console.log(filter);
    };
    SearchitemComponent.prototype.updateAmount = function (food) {
        food.productId = food.id;
        var productId = food.productId;
        var id = food.id;
        if (food.grams != 0) {
            var energy = (food.grams / 100) * food.energyKcal;
        }
        if (food.amount != 0) {
            var energy = (food.foodMass * (food.amount / 100)) * food.energyKcal;
        }
        console.log(energy);
        for (var i = 0; i < this.diaryitems.length; i++) {
            if (this.diaryitems[i].id == id) {
                this.diaryitems[i].energy = energy;
                if (this.diaryitems[i].grams != 0) {
                    this.diaryitems[i].info = this.diaryitems[i].grams + ' g';
                }
                if (this.diaryitems[i].amount != 0) {
                    this.diaryitems[i].info = this.diaryitems[i].amount + ' portion(s)';
                }
                this.diaryitems[i].selecting = false;
            }
        }
        localStorage[this.diaryDate] = JSON.stringify(this.diaryitems);
        /// localStorage add !!!
        /// remove descriptions amountdescription/gramsdescription
        /// delete diaryitem
    };
    SearchitemComponent.prototype.addAmount = function (food) {
        food.productId = food.id;
        var productId = food.productId;
        var foodMass = food.units[1].mass;
        console.log(food.productId);
        console.log(food.amount);
        console.log(food.grams);
        var diaryDate = this.diaryDate;
        if (food.grams != 0) {
            var energy = (food.grams / 100) * food.energyKcal;
        }
        if (food.amount != 0) {
            var energy = (foodMass * (food.amount / 100)) * food.energyKcal;
        }
        var unixtimestamp = Math.round(+new Date() / 1000);
        var storageItems = localStorage[diaryDate];
        if (storageItems) {
            storageItems = JSON.parse(storageItems);
            var foodTest = {
                id: unixtimestamp,
                energyKcal: food.energyKcal,
                foodMass: foodMass,
                foodName: food.name.fi,
                food: food.id,
                energy: energy,
                amount: food.amount,
                grams: food.grams
            };
            storageItems.push(foodTest);
            localStorage[diaryDate] = JSON.stringify(storageItems);
        }
        else {
            var storageItems_1 = new Array();
            var foodTest = {
                id: unixtimestamp,
                energyKcal: food.energyKcal,
                foodMass: foodMass,
                foodName: food.name.fi,
                food: food.id,
                energy: energy,
                amount: food.amount,
                grams: food.grams
            };
            storageItems_1.push(foodTest);
            localStorage[diaryDate] = JSON.stringify(storageItems_1);
        }
        this.diaryitems.push(foodTest);
        for (var i = 0; i < this.diaryitems.length; i++) {
            if (this.diaryitems[i].grams != 0) {
                this.diaryitems[i].info = this.diaryitems[i].grams + ' g';
            }
            if (this.diaryitems[i].amount != 0) {
                this.diaryitems[i].info = this.diaryitems[i].amount + ' portion(s)';
            }
            if (food.grams != 0) {
                var energy = (food.grams / 100) * food.energyKcal;
            }
            if (food.amount != 0) {
                var energy = (food.units[1].mass * (food.amount / 100)) * food.energyKcal;
            }
        }
        /*
                  let foodTest = {
                    food: food.id,
                    energy: energy,
                    amount: food.amount,
                    grams: food.grams
                  };
        
                  let mystorageItems = new Array();
                  mystorageItems.push(foodTest);
        
                  localStorage[this.diaryDate] = JSON.stringify(mystorageItems);
        */
        //let storageItems = JSON.parse(localStorage["mystorageItems"]);
        //let service = this.dataService.createFood(food);
    };
    SearchitemComponent.prototype.filterEmployees = function (filter) {
        console.log(filter);
        var allEmployees = this.employees;
        if (filter.length == 0) {
            this.employees = allEmployees;
            this.pageButtons = this.allPageButtonsInit;
            this.employees = this.allEmployees.slice(0, 5);
        }
        else {
            this.employees = this.employeesFilter;
            console.log(this.employeesFilter);
            this.employees = this.employeesFilter.filter(function (item) { return item.nimi.toLowerCase().indexOf(filter.toLowerCase()) > -1 ||
                item.email.toLowerCase().indexOf(filter.toLowerCase()) > -1; });
            this.allFoundEmployees = this.employees;
            this.allPageButtons = this.pageButtons;
            this.employees = this.employees.slice(0, 5);
            var buttonPagesAmount = this.allFoundEmployees.length / 5;
            buttonPagesAmount = Math.ceil(buttonPagesAmount);
            this.pageButtons = [];
            console.log(buttonPagesAmount);
            var firstPage = -5;
            var lastPage = 0;
            for (var i = 0; i < buttonPagesAmount; i++) {
                firstPage = firstPage + 5;
                lastPage = lastPage + 5;
                var pageButton = {
                    filter: true,
                    id: i,
                    firstPage: firstPage,
                    lastPage: lastPage
                };
                this.pageButtons.push(pageButton);
            }
        }
    };
    SearchitemComponent.prototype.filterByFirstOrLastName = function (filterString) {
        this.employees = this.employees.filter(function (filter) { return filter.firstName == filterString; });
    };
    SearchitemComponent.prototype.openAddForm = function () {
        if (!this.addFormOpen) {
            this.addFormOpen = true;
        }
        else {
            this.addFormOpen = false;
        }
    };
    SearchitemComponent.prototype.changeDay = function (date) {
        console.log(date);
        this.diaryDate = date;
        for (var i = 0; i < this.dayButtons.length; i++) {
            this.dayButtons[i].diaryDateActive = false;
            if (this.diaryDate === this.dayButtons[i].fulldate) {
                this.dayButtons[i].diaryDateActive = true;
            }
        }
        var storageItems = localStorage[this.diaryDate];
        if (storageItems) {
            storageItems = JSON.parse(storageItems);
            console.log(storageItems);
            this.diaryitems = storageItems;
            for (var i = 0; i < this.diaryitems.length; i++) {
                this.diaryitems[i].selecting = false;
                if (this.diaryitems[i].grams != 0) {
                    this.diaryitems[i].info = this.diaryitems[i].grams + ' g';
                }
                if (this.diaryitems[i].amount != 0) {
                    this.diaryitems[i].info = this.diaryitems[i].amount + ' portion(s)';
                }
            }
        }
        else {
            this.diaryitems = [];
        }
    };
    SearchitemComponent.prototype.changePage = function (firstPage, filter) {
        if (filter) {
            console.log('Filter is on!');
            this.searchitems = this.allSearchitems.slice(firstPage, firstPage + 5);
        }
        else {
            this.searchitems = this.allSearchitems.slice(firstPage, firstPage + 5);
        }
    };
    SearchitemComponent.prototype.checkSearchItem = function (employee) {
        if (employee.selecting) {
            employee.selecting = false;
        }
        else {
            employee.selecting = true;
        }
        this.selectedEmployee = employee;
    };
    SearchitemComponent.prototype.checkItem = function (foodItem) {
        if (foodItem.selecting) {
            foodItem.selecting = false;
        }
        else {
            foodItem.selecting = true;
        }
        this.foodItem = foodItem;
    };
    SearchitemComponent.prototype.newEmployee = function (nimi, email, opiskelijanumero, opintopisteet) {
        var _this = this;
        this.dataService.createEmployee(nimi, email, opiskelijanumero, opintopisteet)
            .then(function (empl) {
            _this.dataService.getEmployees().subscribe(function (employees) {
                if (document.getElementById('filter').value !== '') {
                    document.getElementById('filter').value = '';
                    _this.ngOnInit();
                }
                else {
                    _this.employees = employees;
                    _this.allEmployees = employees;
                    _this.employeesFilter = _this.allEmployees;
                    console.log(_this.allEmployees);
                    _this.employees = _this.allEmployees.slice(0, 5);
                    var buttonPagesAmount = (_this.allEmployees.length / 5) - 0.2;
                    console.log(buttonPagesAmount);
                    if (Number.isInteger(buttonPagesAmount)) {
                        console.log('Whole!');
                        console.log(_this.allEmployees.length);
                        var lastItem_1 = _this.pageButtons.length - 1;
                        var lastItemId = _this.pageButtons[lastItem_1].id + 1;
                        var firstPage = _this.pageButtons[lastItem_1].firstPage + 5;
                        var lastPage = _this.pageButtons[lastItem_1].lastPage + 5;
                        var pageButton = {
                            id: lastItemId,
                            firstPage: firstPage,
                            lastPage: lastPage
                        };
                        _this.pageButtons.push(pageButton);
                        console.log(_this.pageButtons);
                    }
                    else {
                        console.log('Not whole!');
                        console.log(_this.allEmployees.length);
                    }
                    var lastItem = _this.pageButtons.length - 1;
                    lastItem = _this.pageButtons[lastItem].firstPage;
                    _this.changePage(lastItem);
                }
            }, Materialize.toast('Employee ' + nimi + 'was added successfully!', 3000, 'green rounded'));
            _this.addFormOpen = false;
        });
    };
    SearchitemComponent.prototype.updateEmployee = function (employee) {
        var _this = this;
        this.selectedEmployee = employee;
        employee.id = employee._id;
        console.log(employee);
        this.dataService.updateEmployee(employee)
            .subscribe(function () {
            employee.editing = false;
            console.log('Employee was updated successfully. ');
            _this.info = 'Employee ' + employee.nimi + ' ' + employee.email + ' was updated successfully!';
            Materialize.toast('Employee edited', 3000, 'green rounded');
        }, function (error) {
            Materialize.toast('Employee edit failed', 3000, 'red rounded');
            console.log('Failed while trying to update the employee. ' + error);
        });
    };
    SearchitemComponent.prototype.removeItem = function (foodItem) {
        var id = foodItem.id;
        var storageItems = localStorage[this.diaryDate];
        console.log(id);
        if (storageItems) {
            storageItems = JSON.parse(storageItems);
            console.log(storageItems);
            this.diaryitems = storageItems;
            for (var i = 0; i < this.diaryitems.length; i++) {
                if (this.diaryitems[i].grams != 0) {
                    this.diaryitems[i].info = this.diaryitems[i].grams + ' g';
                }
                if (this.diaryitems[i].amount != 0) {
                    this.diaryitems[i].info = this.diaryitems[i].amount + ' portion(s)';
                }
                if (this.diaryitems[i].id == id) {
                    this.diaryitems[i].info = 'getting removed!';
                    this.diaryitems.splice(i, 1);
                }
            }
        }
        localStorage[this.diaryDate] = JSON.stringify(this.diaryitems);
    };
    SearchitemComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'searchitems',
            templateUrl: 'searchitem.component.html'
        }), 
        __metadata('design:paramtypes', [data_service_1.DataService])
    ], SearchitemComponent);
    return SearchitemComponent;
}());
exports.SearchitemComponent = SearchitemComponent;
//# sourceMappingURL=searchitem.component.js.map